#include <iostream>
#include <fstream>
#include <vector>
#include <utility>
#include <map>
#include <stack>
#include <queue>
#include <cstdlib>
#include <thread>
#include <mutex>


std::mutex mut;

std::mutex *bcMut;

//old -> new
std::map<int, int> vertexMapping;
//new -> old
std::vector<int>  rVertexMapping;
int verticesNum = 0;
int vToDo = 0;
std::vector<double> BC;
std::vector<std::vector<int>> outEdges;

int mapVertex(int v) {
	std::map<int, int>::iterator it;
	it = vertexMapping.find(v);
	if (it == vertexMapping.end()) {
		vertexMapping[v] = verticesNum;
		rVertexMapping.push_back(v);
		outEdges.push_back(std::vector<int>());
		verticesNum++;
		return verticesNum - 1;
	}

	return it->second;
}

void readInput(std::string filePath) {
	std::ifstream myFile(filePath);
	std::map<int,int>::iterator it;

	int a, b, mappedA, mappedB;

	while(myFile >> a) {
		myFile >> b;

		mappedA = mapVertex(a);
		mappedB = mapVertex(b);

		outEdges[mappedA].push_back(mappedB);
	}
	myFile.close();
}

void printOutEdges() {
	for (int i = 0; i < verticesNum; i++) {
		std::cout << "v: " << rVertexMapping[i] << "|";
		for (auto it = outEdges[i].begin(); it != outEdges[i].end(); ++it) {
			std::cout << " " << rVertexMapping[*it];
		}

		std::cout << std::endl;
	}
}

void printBetweenness(std::string filePath) {
	std::map<int, double> results;

	for (int i = 0; i < verticesNum; i++) {
		if (!outEdges[i].empty()) {
			results[rVertexMapping[i]] = BC[i];
		}
	}

	std::ofstream myFile(filePath);

	for (auto it = results.begin(); it != results.end(); it++) {
		myFile << it->first << " " << it->second << "\n";
	}

	myFile.close();
}

void brandesConcurrent() {
	std::vector<std::vector<int>> P(verticesNum);
	std::vector<int> sigma(verticesNum);
	std::vector<int> d(verticesNum);
	std::vector<double> delta(verticesNum);
	bool verticesLeft = true;
	int s;

	while (verticesLeft) {
		mut.lock();
		if (vToDo == verticesNum) {
			verticesLeft = false;
			mut.unlock();
		}
		else {
			s = vToDo;
			vToDo++;
			mut.unlock();

			std::stack<int> S;
			for (int w = 0; w < verticesNum; w++) {
				P[w] = std::vector<int>();
				sigma[w] = 0;
				d[w] = -1;
				delta[w] = 0;
			}

			sigma[s] = 1;
			d[s] = 0;
			std::queue<int> Q;
			Q.push(s);

			while(!Q.empty()) {
				int v = Q.front();
				Q.pop();
				S.push(v);

				//for each neighbor
				for (auto w = outEdges[v].begin(); w != outEdges[v].end(); ++w) {
					if (d[*w] < 0) {
						Q.push(*w);
						d[*w] = d[v] + 1;
					}

					if (d[*w] == d[v] + 1) {
						sigma[*w] += sigma[v];
						P[*w].push_back(v);
					}
				}

			}

			while (!S.empty()) {
				int w = S.top();
				S.pop();
				for (auto v = P[w].begin(); v != P[w].end(); ++v) {
					delta[*v] += (double(sigma[*v]) / sigma[w])*(1+ delta[w]);
				}


				if (w != s) {
					bcMut[w].lock();
					BC[w] += delta[w];
					bcMut[w].unlock();
				}
			}

		}
	}
}

int main(int argc, char* argv[])
{
	int threadsNum = atoi(argv[1]);
	std::string inputFile = argv[2];
	std::string outputFile = argv[3];

	readInput(inputFile);

	std::thread t[threadsNum];

	BC.resize(verticesNum);
	bcMut = new std::mutex[verticesNum];
	for (int v = 0; v < verticesNum; v++) {
		BC[v] = 0;
	}

	for (int i = 0; i < threadsNum; i++) {
        t[i] = std::thread(brandesConcurrent);
	}

	for (int i = 0; i < threadsNum; i++) {
        t[i].join();
	}

	printBetweenness(outputFile);

	delete[] bcMut;
	return 0;
}
