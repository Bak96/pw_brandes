#!/bin/bash

program=""
l_watkow=0
plikIn=""
plikOut=""
EXIT_FAILURE=1
MIN_LEN=8
act=1

useage() {
    echo "Useage: bash time.sh program plikIn plikOut"
}

parse_arguments(){
    if [[ $# -ne 3 ]]; then
        echo "Illegal number of parameters"
        useage
        exit 1
    else
        program=$1
        plikIn=$2
        plikOut=$3
    fi;
}

run_script(){
    for (( i=1; $i <= 8; i++ )) ; do
           echo -n "./$program $i $plikIn $plikOut"
           time $program $i $plikIn $plikOut
           echo ""
    done
}

parse_arguments $@
run_script